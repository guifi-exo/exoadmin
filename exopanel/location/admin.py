from django.contrib import admin

from . import models

admin.site.register(models.Province)

@admin.register(models.Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    list_display = ('province', 'name')

admin.site.register(models.Netzone)
