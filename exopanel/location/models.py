from django.db import models
from django.utils.translation import gettext as _

class Province(models.Model):

    name = models.CharField(
        _('name'),
        max_length=50,
        unique=True
    )

    class Meta:
        verbose_name = _('Province')
        verbose_name_plural = _('Provinces')

    def __str__(self):
        return self.name


class Municipality(models.Model):

    name = models.CharField(
        _('name'),
        max_length=50,
        unique=True
    )

    province = models.ForeignKey(
        Province,
        verbose_name=_('Province'),
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('Municipality')
        verbose_name_plural = _('Municipalities')

    def __str__(self):
        return '{}, {}'.format(self.province, self.name)

class Netzone(models.Model):

    name = models.CharField(
        _('Netzone'),
        max_length=50,
        unique=True
    )

    description = models.TextField(
        _('Description'),
        max_length=300,
        blank=True,
    )

    class Meta:
        verbose_name = _('Netzone')
        verbose_name_plural = _('Netzones')

    def __str__(self):
        return self.name
